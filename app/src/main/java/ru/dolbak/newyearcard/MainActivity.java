package ru.dolbak.newyearcard;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioAttributes;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    AudioPlayer audioPlayer;
    AudioAttributes attributes;
    SoundPool sPool;
    boolean loaded = false;
    int soundID1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        audioPlayer = new AudioPlayer();
        attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        sPool = new SoundPool.Builder()
                .setAudioAttributes(attributes).build();
        sPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int i, int i1) {
                loaded = true;
            }
        });
        soundID1 = sPool.load(this, R.raw.door_close, 1);
    }

    public void onClick1(View view) {
        audioPlayer.play(this);
    }

    public void onClick2(View view) {
        audioPlayer.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        audioPlayer.stop();
    }

    public void onClick3(View view) {
        if (loaded){
            int streamID1 = sPool.play(soundID1, 1.0f, 1.0f, 1, 0, 1);
        }
    }
}